package ccgg.HibernateDemo;

import org.hibernate.Session;
import org.hibernate.Transaction;

import beans.User;
import util.HibernateUtil;

public class Test1A {

	public static void main(String[] args) {
		// session is Persistent until close
		Session session = HibernateUtil.currentSession();
		User user = (User) session.load(User.class, "HBob");
		System.out.println(user);
		HibernateUtil.closeSession();
		// when session is closed, user is Detached. but can be modified (change value
		// in Hibernate cache - local)
		user.setAge(27);
		
		// open session2 in Hibernate (main memory)
		Session session2 = HibernateUtil.currentSession();
		session2.merge(user);
		user = (User) session2.load(User.class, "HBob");
		System.out.println(user);
		
		// write to database (disk) not on cache or Hibernate
		Transaction tx = session2.beginTransaction();
		tx.commit();  // sync session(HBob, 27) and database(HBob, 18). rollback
		HibernateUtil.closeSession();
	}

}
