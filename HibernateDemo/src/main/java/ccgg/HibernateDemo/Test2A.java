package ccgg.HibernateDemo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.CallableStatement;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.jdbc.Work;


import beans.User;
import util.HibernateUtil;

public class Test2A {

	public static void main(String[] args) {
		Session session = HibernateUtil.currentSession();
		Query query = session.getNamedQuery("userSP");
		
		List<User> list = query.list();
		for(User u : list)
			System.out.println(u);

		System.out.println("*******************");
		
		// Connection conn = session.connection();
		session.doWork(new Work() {
			public void execute(Connection conn) throws SQLException {
				String sp = "{call queryuser()}";
				CallableStatement cs = conn.prepareCall(sp);
				// cs.registerOutputParameter(1, mysqltype.cursor);
				cs.execute();
				/*
				 * ResultSet res = cs.getObject(1); wile(rs.next()){
				 * System.out.println(rs.getString("name")+ "\t" + rs.getInt("age")); }
				 * rs.close();
				 */
			}
		});
		
		HibernateUtil.closeSession();
		
	}

}
