package ccgg.HibernateDemo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import beans.User;

public class Test1B {

	public static void main(String[] args) {
		Configuration cfg = new Configuration().configure();
		SessionFactory factory = cfg.buildSessionFactory();
		
		// First Session
		Session session1 = factory.openSession();
		User user1 = (User) session1.load(User.class, "Tommy");
		System.out.println(user1);
		session1.close();
		
		try {
			Thread.sleep(6000);  // delay 6s to make cache expire
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		Session sessionTest = factory.openSession();
		user1 = (User) sessionTest.load(User.class, "Tommy");
		System.out.println(user1);
		
		
		
		// Second session
		Session session2 = factory.openSession();
		// The query is running again because of no caching after 6 seconds
		User user2 = (User) session2.load(User.class, "HBob");
		System.out.println(user2);
		session2.close();
		
		// Third Session 
		Session session3 = factory.openSession();
		// The query is not running because of caching on SessionFactory
		User user3 = (User) session3.load(User.class, "HBob");
		System.out.println(user3);
		// because create a new object User copying the object in cache 
		System.out.println("user2==user3 ?" + (user2==user3));
		System.out.println(user2.getName()==user3.getName());
		session3.close();

		// Fourth Session
		Session session4 = factory.openSession();
		// still from cache, because we set cache to 5 seconds' idle time
		User user4 = (User) session4.load(User.class, "HBob");
		System.out.println("user3==user4 ?" + (user3==user4));
		session4.close();
		
	}

}
