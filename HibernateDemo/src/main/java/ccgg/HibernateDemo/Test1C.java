package ccgg.HibernateDemo;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import beans.User;
import util.HibernateUtil;

public class Test1C {

	public static void main(String[] args) {
		// open a session and make it as a singleton (design pattern)
		Session session = HibernateUtil.currentSession();
		String hql = "from User";
		Query query = session.createQuery(hql);
		
		// default false
		query.setCacheable(true);
		
		List<User> list = query.list();
		for(User u : list){
			System.out.println(u);
		}
		System.out.println("~~~~ second time printing: ");
		List<User> list2 = query.list();
		for(User u : list2){
			System.out.println(u);
		}
		
		HibernateUtil.closeSession();
	}

}
