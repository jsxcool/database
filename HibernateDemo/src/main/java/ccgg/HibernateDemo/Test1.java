package ccgg.HibernateDemo;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import beans.User;
import util.HibernateUtil;


public class Test1 {

	public static void main(String[] args) {
		
		Session session =  HibernateUtil.currentSession();
		// rollback: add a set of data, if one fails, it rolls back to delete some already added data
		Transaction tx = session.beginTransaction();
		
		// user connects to session
		 User user = new User("Scott", 18);
		// persistent: session continues existing, until .closeSession()
		 session.save(user);  
		 tx.commit();
		
		// hql: hibernate query language
		String hql = "from User";
		// from User = select * from User
		
		Query query = session.createQuery(hql);
		List<User> list = query.list();
		
		for(User u : list) {
			System.out.println(u);
		}
		
		HibernateUtil.closeSession();
		
	}

}
