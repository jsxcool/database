package ccgg.HibernateDemo;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import beans.User;
import util.HibernateUtil;

public class Test2D {

	public static void main(String[] args) {
		Session session = HibernateUtil.currentSession();
		
		// How to update a record in this table
		// First way: Use HQL or SQL to update it
		String hql = "update User set age=:age where name=:name";
		Transaction tx = session.beginTransaction();
		Query query = session.createQuery(hql);
		query.setInteger("age", 25);
		query.setString("name", "Tracy");
		query.executeUpdate();
		tx.commit();
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
		// Second way: User load or get method and then apply setter
		tx = session.beginTransaction();
		User user = (User) session.load(User.class, "Tommy");  // also get method
		user.setAge(50);
		tx.commit();
		
		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		
		// Third way: Similar to second way, I use Criteria instead
		Criteria ct = session.createCriteria(User.class);
		tx = session.beginTransaction();
		User user2 = (User) ct.add(Restrictions.eq("name", "Scott")).uniqueResult();
		user2.setAge(70);
		tx.commit();
		session.close();

	}

}
