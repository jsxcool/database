package util;

//The lifecycle of a Session is bounded by the beginning and end of a logical transaction.
// (Long transactions might span several database transactions.)
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

// Hibernate is a packaging shell of jdbc
public class HibernateUtil {
	// a factory of sessions
	private static final SessionFactory FACTORY;
	
	static {
		try {
			// .configure()
			// Use the mappings and properties specified in an application resource 
			// named 'hibernate.cfg.xml'.
			FACTORY = new Configuration().configure().buildSessionFactory();
			System.out.println("connection success");
		} catch(Exception e) {
			throw new RuntimeException(e.getMessage(), e);
		}
	}
	
	private static final ThreadLocal<Session> SESSION = new ThreadLocal<Session>() {
		@Override
		protected Session initialValue() {
			return FACTORY.openSession();
		}
	};

	public static SessionFactory getSessionFactory() {
		return FACTORY;
	}
	
	public static Session currentSession() {
		// return the thread-local variable -- FACTORY.openSession()
		Session s = SESSION.get();
		if (s == null) {
			s = FACTORY.openSession();
			// set the thread-local variable
			SESSION.set(s);
		}
		return s;
	}
	
	public static void closeSession() {
		Session s = SESSION.get();
		SESSION.set(null);
		if(s != null) {
			s.close();
		}
	}

	
}